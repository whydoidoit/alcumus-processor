const {Lock: {lock}} = require('alcumus-data-source-server');

module.exports = {
    async using(locks, fn) {
        let release = await module.exports.lock(locks);
        try {
            return await Promise.resolve(fn(release.extend));
        } finally {
            try {
                release();
            } catch(e) {
                console.error(`Error releasing locks:\n\n${e.stack}`);
            }
        }
    },
    async lock(locks) {
        locks = Array.isArray(locks) ? locks : [locks];
        let toRelease = await Promise.all(locks.map(key => lock(key)));
        let result = async () => {
            await Promise.all(toRelease.map(fn => fn()));
        };
        result.extend = (time) => {
            return Promise.all(toRelease.map(fn=>fn.extend(time)));
        };
        result.extend.locks = toRelease;
        return result;
    }
};
