/* globals describe, beforeEach, afterEach, it */

const sinon = require('sinon');
require('rewiremock/node');
const rewiremock = require('rewiremock').default;
const {expect} = require('chai');
require('chai').use(require('sinon-chai'));

let mocks = {};
rewiremock('alcumus-app-datasource-mutations')
    .mockThrough((name, value) => {
        return typeof value === 'function' ? mocks[name] = mocks[name] || sinon.spy(function (...params) {
            return value(...params);
        }) : value;
    });


describe('Behaviour object updater', function () {
    beforeEach(function () {
        rewiremock.enable();
        Object.keys(mocks).forEach(mock => mocks[mock].resetHistory());
    });
    afterEach(function () {
        rewiremock.disable();
    });

    it('should create mutations for differing objects', function () {
        const {set} = require('alcumus-app-datasource-mutations');
        let update = require('./behaviour-object-updater');
        update({}, {a: 1});
        expect(set.calledOnce).to.be.true;
    });
    it('should not create mutations for similar objects', function () {
        const {set} = require('alcumus-app-datasource-mutations');
        let update = require('./behaviour-object-updater');
        update({a: 1}, {a: 1});
        expect(set.called).to.be.false;
    });
    it('should not create a mutation for private properties', function () {
        const {set} = require('alcumus-app-datasource-mutations');
        let update = require('./behaviour-object-updater');
        update({a: 1}, {a: 1, _b: 2});
        expect(set.called).to.be.false;
    });
    it("should have a 'state' modification for _behaviours.state", function () {
        let update = require('./behaviour-object-updater');
        let result = update({
            a: 1,
            _behaviours: {_state: 'a'}
        }, {
            a: 1,
            _behaviours: {_state: 'b'}
        });
        expect(result.path).to.eq('behaviours.state');
    });
    it('should update behaviour instances', function () {
        const {set} = require('alcumus-app-datasource-mutations');
        let update = require('./behaviour-object-updater');
        let result = update({
            a: 1,
            _behaviours: {
                _state: 'a',
                instances: {a: [{a: 1}], b: [{a: 1}]}
            }
        }, {
            a: 1,
            _behaviours: {
                _state: 'a',
                instances: {a: [{a: 1}, {b: 2}], b: [{a: 1}]}
            }
        });
        expect(result.path).to.match(/behaviours.instances\[a]/);
        expect(set.calledOnce).to.be.true;
    });
    it('should update behaviours even if they were missing in the original', function () {
        const {set} = require('alcumus-app-datasource-mutations');
        let update = require('./behaviour-object-updater');
        let result = update({
            a: 1,
            _behaviours: {
            }
        }, {
            a: 1,
            _behaviours: {
                _state: 'a',
                instances: {a: [{a: 1}, {b: 2}], b: [{a: 1}]}
            }
        });
        expect(result.path).to.match(/behaviours.instances\[b]/);
        expect(set.calledThrice).to.be.true;
    });

});
