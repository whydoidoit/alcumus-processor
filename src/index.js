const Locks = require('./locks');
const processor = require('./processor');
const {initialize, run, process, handleProcess, handleEnqueue} = require('./process');
const Data = require('./data');
const events = require('./events');
const queue = require('./queue')

function dedupe(fn, dedupe) {
    fn.dedupe = dedupe;
    return fn;
}

module.exports = {
    Locks,
    processor,
    initialize,
    handleProcess,
    handleEnqueue,
    run,
    process,
    dedupe,
    queue,
    Data,
    events
};
