/* globals describe, it */

const {using} = require('./locks');
const {expect} = require('chai');

describe('Multi-key locks', function() {
    it('should be able to lock on multiple keys', async function () {
        let counter = 0;
        await using(['a','b','c','d','e'], async ()=>{
            counter++;
        });
        expect(counter).to.eq(1);
    });
    it('should be able to lock on a single key', async function () {
        let counter = 0;
        await using('a', async () => {
            counter++;
        });
        expect(counter).to.eq(1);
    });
});
